package ru.vsu.jmbpm.jsf.test;

import org.junit.Assert;
import org.junit.Test;

import ru.vsu.jmbpm.jsf.form.parse.FormulaParser;

public class FormulaTest extends Assert 
{

	private static final String correctString = "#{bean.form.fields['a']+bean.form.fields['b']}";
	private static final String testString = "={a}+{b}";
	
	@Test
	public void formulaCorrectTest()
	{
		FormulaParser parser = new FormulaParser("bean");
		assertEquals(correctString, parser.parse(testString));
	}
}
