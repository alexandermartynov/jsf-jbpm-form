package ru.vsu.jmbpm.jsf.form.parse;

import java.util.HashMap;
import java.util.Map;

public class ParsedField implements Comparable<ParsedField>
{
	private int position;
	private String variableName;
	private String type;
	private String lable;
	private Class<?> fieldClass;
	private Boolean readonly;
	private Boolean required;
	private Map<String, String> selected = new HashMap<String, String>();
	private Boolean input = false;
	private String bindingName;
	private String formula;
	
	public Boolean getRequired() 
	{
		return required;
	}

	public void setRequired(Boolean required) 
	{
		this.required = required;
	}

	public int getPosition()
	{
		return position;
	}
	
	public void setPosition(int position)
	{
		this.position = position;
	}
	
	public String getVariableName() 
	{
		return variableName;
	}
	
	public void setVariableName(String variableName)
	{
		this.variableName = variableName;
	}
	
	public String getType()
	{
		return type;
	}
	
	public void setType(String type) 
	{
		this.type = type;
	}
	
	public String getLable()
	{
		return lable;
	}
	
	public void setLable(String lable)
	{
		this.lable = lable;
	}
	
	public Class<?> getFieldClass() 
	{
		return fieldClass;
	}
	
	public void setFieldClass(Class<?> fieldClass)
	{
		this.fieldClass = fieldClass;
	}

	public Boolean getReadonly() 
	{
		return readonly;
	}

	public void setReadonly(Boolean readonly) 
	{
		this.readonly = readonly;
	}

	@Override
	public int compareTo(ParsedField o) 
	{
		if(getPosition() > o.getPosition())
		{
			return 1;
		}
		if(getPosition() < o.getPosition())
		{
			return -1;
		}
		return 0;
	}

	public Map<String, String> getSelected() 
	{
		return selected;
	}

	public void setSelected(Map<String, String> selected)
	{
		this.selected = selected;
	}

	public Boolean isInput()
	{
		return input == null? true : input;
	}

	public void setInput(Boolean input) 
	{
		this.input = input;
	}

	public String getBindingName() 
	{
		return bindingName;
	}

	public void setBindingName(String bindingName) 
	{
		this.bindingName = bindingName;
	}

	public String getFormula() 
	{
		return formula;
	}

	public void setFormula(String formula)
	{
		this.formula = formula;
	}
		
}
