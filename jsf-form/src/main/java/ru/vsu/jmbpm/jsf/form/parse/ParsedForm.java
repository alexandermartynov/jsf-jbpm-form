package ru.vsu.jmbpm.jsf.form.parse;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ParsedForm implements Iterable<ParsedField>
{
	private List<ParsedField> fields = new ArrayList<ParsedField>();
	
	public List<ParsedField> getFields()
	{
		return fields;
	}

	@Override
	public Iterator<ParsedField> iterator()
	{
		return fields.iterator();
	}
}
