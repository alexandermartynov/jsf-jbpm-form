package ru.vsu.jmbpm.jsf.form.parse;

import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FormParser implements Parser
{
	private static final String FIELD = "field";
	private static final String PROPERTY = "property";
	private static final String PROPERTY_NAME = "name";
	private static final String PROPERTY_VALUE = "value";
	
	private static final String SELECT_FIELD = "SelectOneMenu";
	private static final String RANGE_FORMULA = "rangeFormula";
	private static final String FORMULA = "formula";
	
	private static final String POSITION = "position";
	private static final String VARIABLE_NAME = "name";
	private static final String TYPE = "type";
	private static final String LABEL = "label";
	private static final String READONLY = "readonly";
	private static final String FIELD_CLASS = "fieldClass";
	private static final String FIELD_REQUIRED = "fieldRequired";
	private static final String HTML_CONTENT = "htmlContent";
	private static final String INPUT_BINDING = "inputBinding";
	private static final String OUTPUT_BINDING = "outputBinding";
	
	@Override
	public ParsedForm parse(InputStream stream) throws Exception
	{
		if(stream == null)
			return new ParsedForm();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(stream);
		return parseForm(doc);
	}
	
	protected String getValue(NamedNodeMap attributes)
	{
		return attributes.getNamedItem(PROPERTY_VALUE).getNodeValue();
	}
	
	protected Map<String, String> getSelectedItems(String rangeFormula)
	{
		Map<String, String> selectedItems = new HashMap<String, String>();
		rangeFormula = rangeFormula.substring(1, rangeFormula.length() - 1);
		String[] items = rangeFormula.split(";");
		String[] keyAndValue;
		for(String item : items)
		{
			keyAndValue = item.split(",");
			selectedItems.put(keyAndValue[0], keyAndValue[1]);
		}
		return selectedItems;
	}
	
	protected ParsedField parseField(Node node) throws ClassNotFoundException
	{
		NodeList children = node.getChildNodes(); 
		ParsedField field = new ParsedField();
		NamedNodeMap attributes = node.getAttributes();
		field.setPosition(Integer.parseInt(attributes.getNamedItem(POSITION).getNodeValue()));
		field.setVariableName(attributes.getNamedItem(VARIABLE_NAME).getNodeValue());
		field.setType(attributes.getNamedItem(TYPE).getNodeValue());
		for (int i=0; i<children.getLength(); i++)
		{
			Node child  = children.item(i);
			if(child.getNodeName().equals(PROPERTY))
			{
				attributes = child.getAttributes();
				switch(attributes.getNamedItem(PROPERTY_NAME).getNodeValue())
				{
					case LABEL: field.setLable(getLabel(getValue(attributes))); break;
					case READONLY: field.setReadonly(Boolean.parseBoolean(getValue(attributes))); break;
					case FIELD_CLASS: field.setFieldClass(Class.forName(getValue(attributes))); break;
					case FIELD_REQUIRED: field.setRequired(Boolean.parseBoolean(getValue(attributes))); break;
					case HTML_CONTENT: field.setLable(getValue(attributes)); break;
					case RANGE_FORMULA: 
						{
							field.setType(SELECT_FIELD);
							field.setSelected(getSelectedItems(getValue(attributes)));
						}
					break;	
					case FORMULA: field.setFormula(getValue(attributes)); break;
					case INPUT_BINDING: 
						{
							field.setInput(true); 
							field.setBindingName(getValue(attributes));
						}
					break;
					case OUTPUT_BINDING:
						{
							field.setInput(false); 
							field.setBindingName(getValue(attributes));
						}
					break;
				}
			}
		}
		return field;
	}
	
	protected String getLabel(String label)
	{
		String result = label.split(";")[3];
		return result.substring(0, result.length()-4);
	}
	
	protected ParsedForm parseForm(Node node) throws ClassNotFoundException
	{
		NodeList children = node.getChildNodes().item(0).getChildNodes(); 
		ParsedForm form = new ParsedForm();
		for (int i=0; i<children.getLength(); i++)
		{
			Node child  = children.item(i);
			if(child.getNodeName().equals(FIELD))
			{
				form.getFields().add(parseField(child));
			}			
		}
		Collections.sort(form.getFields());
		return form;
	}
}
