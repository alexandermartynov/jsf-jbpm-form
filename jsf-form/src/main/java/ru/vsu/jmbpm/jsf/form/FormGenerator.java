package ru.vsu.jmbpm.jsf.form;

import java.io.InputStream;
import java.util.Map;

import javax.faces.context.FacesContext;

import ru.vsu.jmbpm.jsf.factory.ComponentsFactory;
import ru.vsu.jmbpm.jsf.factory.ComponentsFactoryBuilder;
import ru.vsu.jmbpm.jsf.form.parse.FormulaParser;
import ru.vsu.jmbpm.jsf.form.parse.ParsedField;
import ru.vsu.jmbpm.jsf.form.parse.ParsedForm;
import ru.vsu.jmbpm.jsf.form.parse.Parser;
import ru.vsu.jmbpm.jsf.form.parse.ParserFactory;

public class FormGenerator 
{	
	protected InputStream getTemplate(String name, TemplateType type)
	{
		InputStream stream = FormGenerator.class.getResourceAsStream("/" + name + "-taskform"+type);
		if (stream != null) 
		{
			return stream;
		} 
		stream = FormGenerator.class.getResourceAsStream("/" + name + type);
		if (stream != null) 
		{
			return stream;
		} 
		return null;
	}
	
	protected Form generate(FacesContext context,  Map<String,Object> inputContext, String beanName, ParsedForm parsedForm)
	{
		ComponentsFactory factory = ComponentsFactoryBuilder.getComponentsFactory();
		FieldGenerator fieldGenerator = new FieldGenerator(factory, context, beanName);
		Form form = new Form(factory.getForm());
		form.addComponent(factory.getMessages());		
		for (ParsedField field : parsedForm)
		{
			switch(field.getType())
			{
				case FieldType.INPUT_TEXT:
				case FieldType.INPUT_TEXT_DOUBLE:
				case FieldType.INPUT_TEXT_INTEGER:
				case FieldType.INPUT_TEXT_FLOAT: form.addComponent(fieldGenerator.getInputText(field)); break;
				case FieldType.INPUT_TEXTARIA: form.addComponent(fieldGenerator.getInputTextaria(field)); break;
				case FieldType.SELECT_ONE_LISTBOX: form.addComponent(fieldGenerator.getSelectOneListBox(field)); break;
				case FieldType.SELECT_ONE_MENU: form.addComponent(fieldGenerator.getSelectOneMenu(field)); break;
				case FieldType.INPUT_DATE: 
					form.addComponent(fieldGenerator.getCalendar(field)); 
				break;
				case FieldType.LABEL: form.addComponent(fieldGenerator.getHtmlLabel(field)); break;
			}
			if(field.isInput())
			{
				form.getFields().put(field.getVariableName(), 
						inputContext.get(field.getBindingName()));
			}			
		}
		form.addComponent(fieldGenerator.getCommandButton("action", "Завершить"));
		return form;
	}
		
	public Form generate(FacesContext context, Map<String,Object> inputContext, String beanName, String name) throws Exception
	{
		Parser formParser = ParserFactory.getFormParser();
		return generate(context, inputContext, beanName, formParser.parse(getTemplate(name, TemplateType.FORM)));
	}
	
	
}
