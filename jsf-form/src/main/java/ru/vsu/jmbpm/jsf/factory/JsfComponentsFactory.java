package ru.vsu.jmbpm.jsf.factory;

import javax.faces.component.UICommand;
import javax.faces.component.UIForm;
import javax.faces.component.UIInput;
import javax.faces.component.UIMessage;
import javax.faces.component.UIMessages;
import javax.faces.component.UIOutput;
import javax.faces.component.UIPanel;
import javax.faces.component.UISelectOne;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlMessage;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlOutputLabel;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlMessages;
import javax.faces.component.html.HtmlInputTextarea;
import javax.faces.component.html.HtmlSelectOneListbox;
import javax.faces.component.html.HtmlSelectOneMenu;

public class JsfComponentsFactory implements ComponentsFactory
{
	@Override
	public UIForm getForm() 
	{
		return new HtmlForm();
	}

	@Override
	public UIInput getInputText()
	{
		return new HtmlInputText();
	}

	@Override
	public UIPanel getPanelGrid() 
	{
		return new HtmlPanelGrid();
	}

	@Override
	public UIOutput getOutputLabel() 
	{
		return new HtmlOutputLabel();
	}

	@Override
	public UICommand getCommandButton() 
	{
		return new HtmlCommandButton();
	}

	@Override
	public UIMessages getMessages()
	{
		return new HtmlMessages();
	}

	@Override
	public UIMessage getMessage() 
	{
		return new HtmlMessage();
	}

	@Override
	public UIInput getInputTextaria() 
	{
		return new HtmlInputTextarea();
	}

	@Override
	public UISelectOne getSelectOneListbox() 
	{
		return new HtmlSelectOneListbox();
	}

	@Override
	public UISelectOne getSelectOneMenu() 
	{
		return new HtmlSelectOneMenu();
	}

	@Override
	public UIInput getCalendar() 
	{		
		return new HtmlInputText();
	}
	
	@Override
	public UIOutput getOutputText()
	{
		return new HtmlOutputText();
	}
}
