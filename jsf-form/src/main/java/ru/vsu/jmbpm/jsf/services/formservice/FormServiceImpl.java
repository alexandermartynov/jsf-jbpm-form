package ru.vsu.jmbpm.jsf.services.formservice;

import java.util.Map;

import javax.ejb.Stateless;
import javax.faces.context.FacesContext;

import ru.vsu.jmbpm.jsf.form.Form;
import ru.vsu.jmbpm.jsf.form.FormGenerator;

@Stateless
public class FormServiceImpl implements FormService
{

	@Override
	public Form generateForm(FacesContext context, Map<String,Object> inputContext,
			String beanName, String name) throws Exception {
		FormGenerator generator = new FormGenerator(); 
		return generator.generate(context, inputContext, beanName, name);
	}
	
}
