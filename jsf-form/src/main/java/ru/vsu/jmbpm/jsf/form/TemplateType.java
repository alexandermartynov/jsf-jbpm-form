package ru.vsu.jmbpm.jsf.form;

public enum TemplateType 
{
	FORM(".form"),
	FTL(".ftl");
	
	private String value;
	
	private  TemplateType(final String value)
	{
		this.value = value;
	}
	
	public String toString()
	{ 
		return value;
	}
}
