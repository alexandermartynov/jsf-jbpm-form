package ru.vsu.jmbpm.jsf.form;

import java.util.Collections;
import java.util.Map.Entry;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIOutput;
import javax.faces.component.UIPanel;
import javax.faces.component.UICommand;
import javax.faces.component.UISelectItem;
import javax.faces.component.UISelectOne;
import javax.faces.component.behavior.AjaxBehavior;
import javax.faces.context.FacesContext;
import javax.faces.validator.RequiredValidator;

import ru.vsu.jmbpm.jsf.factory.ComponentsFactory;
import ru.vsu.jmbpm.jsf.factory.ConverterFactory;
import ru.vsu.jmbpm.jsf.form.parse.FormulaParser;
import ru.vsu.jmbpm.jsf.form.parse.ParsedField;

public class FieldGenerator 
{
	private static final String FOR = "for";
	private static final String VALUE = "value";
	private static final String READONLY = "readonly";
	private static final String NAME = "name";
	
	private ComponentsFactory factory;
	private String beanName;
	private ExpressionFactory expressionFactory; 
	private ELContext elContext; 
	private FormulaParser formulaParser;
	private FacesContext context;
	
	protected String getValueExpression(String name)
	{
		StringBuilder strb = new StringBuilder();
		strb.append('#')
			.append('{')
			.append(beanName)
			.append(".form.fields['")
			.append(name)
			.append("']}");
		return strb.toString();
	}
	
	protected ValueExpression createValueExpression(String expression, Class<?> clazz)
	{
		return expressionFactory.createValueExpression(elContext, expression, clazz);
	}
	
	protected MethodExpression createMethodExpression(String action)
	{
		StringBuilder strb = new StringBuilder();
		strb.append('#')
			.append('{')
			.append(beanName)
			.append('.')
			.append(action)
			.append("()}");
		return expressionFactory.createMethodExpression(elContext, strb.toString(), Void.class, new Class[0]);
	}
	
	protected UIInput fillUIInput(UIInput input, ParsedField field)
	{
		AjaxBehavior ajax =(AjaxBehavior)this.context.getApplication().createBehavior(AjaxBehavior.BEHAVIOR_ID);
		ajax.setRender(Collections.singletonList("jsf-jmbpmn-form"));
		input.addClientBehavior("change", ajax);
		input.setId(field.getVariableName());
		input.getAttributes().put(NAME, field.getLable());
		input.getAttributes().put(READONLY, field.getReadonly());
		input.setValueExpression(VALUE, 
				createValueExpression(field.getFormula() == null ? 
						getValueExpression(field.getVariableName()) 
						: formulaParser.parse(field.getFormula()), field.getFieldClass()));
		if(field.getRequired())
		{
			input.addValidator(new RequiredValidator());
		}
		if(!field.getFieldClass().equals(String.class))
		{
			input.setConverter(ConverterFactory.getConvertor(field.getFieldClass()));
		}
		return input;
	}
	
	protected UISelectOne fillUISelectOne(UISelectOne selectOne, ParsedField field)
	{
		selectOne.setId(field.getVariableName());
		selectOne.getAttributes().put(NAME, field.getLable());
		selectOne.setValueExpression(VALUE, 
				createValueExpression(getValueExpression(field.getVariableName()), field.getFieldClass()));
		for (Entry<String,String> item : field.getSelected().entrySet())
		{
			UISelectItem selectItem = new UISelectItem();
			selectItem.setItemValue(item.getKey());
			selectItem.setItemLabel(item.getValue());
			selectOne.getChildren().add(selectItem);
		}
		return selectOne;
	}
	
	public FieldGenerator(ComponentsFactory factory, FacesContext context, String beanName)
	{
		this.factory = factory;
		this.beanName = beanName;
		this.expressionFactory = context.getApplication().getExpressionFactory();
		this.context = context;
		this.elContext = this.context.getELContext();
		this.formulaParser = new FormulaParser(beanName);
	}
	
	public UIComponent getLabel(ParsedField field)
	{
		UIOutput label = factory.getOutputLabel();
		label.getAttributes().put(FOR, field.getVariableName());
		label.setValue(field.getLable());
		return label;
	}
	
	public UIComponent getInputText(ParsedField field)
	{
		UIPanel panel = factory.getPanelGrid();
		UIInput input = factory.getInputText();	
		panel.getChildren().add(getLabel(field));
		panel.getChildren().add(fillUIInput(input, field));
		return panel;
	}
	
	public UICommand getCommandButton(String action, String value)
	{
		UICommand button = factory.getCommandButton();
		button.setValue(value);
		button.setActionExpression(createMethodExpression(action));
		return button;
	}
	
	public UIComponent getCommandButtonsPanel(String confirmAction, String confirmValue,
			String canelAction, String canelValue)
	{
		UIPanel panel = factory.getPanelGrid();
		panel.getChildren().add(getCommandButton(confirmAction, confirmValue));
		panel.getChildren().add(getCommandButton(canelAction, canelValue));
		return panel;
	}
	
	
	public UIComponent getInputTextaria(ParsedField field)
	{
		UIPanel panel = factory.getPanelGrid();
		UIInput input = factory.getInputTextaria();
		panel.getChildren().add(getLabel(field));
		panel.getChildren().add(fillUIInput(input, field));
		return panel;
	}
	
	public UIComponent getSelectOneListBox(ParsedField field)
	{
		UIPanel panel = factory.getPanelGrid();
		UISelectOne selectOne = factory.getSelectOneListbox();
		panel.getChildren().add(getLabel(field));
		panel.getChildren().add(fillUISelectOne(selectOne, field));
		return panel;
	}
	
	public UIComponent getSelectOneMenu(ParsedField field)
	{
		UIPanel panel = factory.getPanelGrid();
		UISelectOne selectOne = factory.getSelectOneMenu();
		panel.getChildren().add(getLabel(field));
		panel.getChildren().add(fillUISelectOne(selectOne, field));
		return panel;
	}
	
	public UIComponent getCalendar(ParsedField field)
	{
		UIPanel panel = factory.getPanelGrid();
		UIInput input = factory.getCalendar();
		panel.getChildren().add(getLabel(field));
		panel.getChildren().add(fillUIInput(input, field));
		return panel;
	}
	
	public UIComponent getHtmlLabel(ParsedField field)
	{
		UIPanel panel = factory.getPanelGrid();
		UIOutput label = factory.getOutputText();
		label.setValue(field.getLable());
		panel.getChildren().add(label);
		return panel;
	}
	
}
