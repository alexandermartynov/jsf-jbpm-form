package ru.vsu.jmbpm.jsf.factory.primefaces;

import javax.faces.component.UICommand;
import javax.faces.component.UIForm;
import javax.faces.component.UIInput;
import javax.faces.component.UIMessage;
import javax.faces.component.UIMessages;
import javax.faces.component.UIOutput;
import javax.faces.component.UIPanel;
import javax.faces.component.UISelectOne;
import javax.faces.component.html.HtmlForm;

import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.message.Message;
import org.primefaces.component.messages.Messages;
import org.primefaces.component.selectonelistbox.SelectOneListbox;
import org.primefaces.component.inputtextarea.InputTextarea;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.mobile.component.field.Field;
import javax.faces.component.html.HtmlOutputText;


import ru.vsu.jmbpm.jsf.factory.ComponentsFactory;

public class PrimefacesComponentsFactory implements ComponentsFactory
{

	@Override
	public UIForm getForm() 
	{
		return new HtmlForm();
	}

	@Override
	public UIInput getInputText() 
	{
		return new InputText();
	}

	@Override
	public UIPanel getPanelGrid() 
	{
		return new Field();
	}

	@Override
	public UIOutput getOutputLabel() 
	{
		return new OutputLabel();
	}

	@Override
	public UICommand getCommandButton() 
	{
		return new CommandButton();
	}

	@Override
	public UIMessages getMessages()
	{
		Messages messages = new Messages();
		messages.setAutoUpdate(true);
		messages.setShowDetail(true);
		messages.setClosable(true);
		return messages;
	}

	@Override
	public UIMessage getMessage()
	{	
		return new Message();
	}

	@Override
	public UIInput getInputTextaria() 
	{
		return new InputTextarea();
	}

	@Override
	public UISelectOne getSelectOneListbox()
	{
		return new SelectOneListbox();
	}

	@Override
	public UISelectOne getSelectOneMenu()
	{
		return new SelectOneMenu();
	}

	@Override
	public UIInput getCalendar() 
	{
		Calendar calendar = new Calendar();
		calendar.setPattern("dd-MM-yyyy");
		return new Calendar();
	}
	
	@Override
	public UIOutput getOutputText()
	{
		return new HtmlOutputText();
	}
}
