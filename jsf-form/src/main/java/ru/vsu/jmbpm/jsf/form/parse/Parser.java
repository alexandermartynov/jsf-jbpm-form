package ru.vsu.jmbpm.jsf.form.parse;

import java.io.InputStream;

public interface Parser 
{
	ParsedForm parse(InputStream stream) throws Exception;
}
