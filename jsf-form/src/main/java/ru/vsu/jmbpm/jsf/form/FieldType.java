package ru.vsu.jmbpm.jsf.form;

public class FieldType 
{
	public static final String INPUT_TEXT = "InputText";
	public static final String INPUT_TEXT_FLOAT = "InputTextFloat";
	public static final String INPUT_TEXTARIA = "InputTextArea";
	public static final String INPUT_TEXT_DOUBLE = "InputTextDouble";
	public static final String SELECT_ONE_LISTBOX = "SelectOneListBox";
	public static final String SELECT_ONE_MENU = "SelectOneMenu";
	public static final String INPUT_DATE = "InputShortDate";
	public static final String INPUT_TEXT_INTEGER = "InputTextInteger";
	public static final String LABEL = "HTMLLabel";
}
