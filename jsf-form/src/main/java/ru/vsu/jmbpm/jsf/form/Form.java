package ru.vsu.jmbpm.jsf.form;

import java.util.HashMap;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;

public class Form
{
	private UIForm form;
	private Map<String,Object> fields = new HashMap<String, Object>();
	
	public Form()
	{
		form = null;
	}
	
	public Form(UIForm form)
	{
		this.form = form;
	}
	
	public UIForm getForm()
	{
		return form;
	}
	
	public void setForm(UIForm form)
	{
		this.form = form;
	}
	
	public void addComponent(UIComponent component)
	{
		form.getChildren().add(component);
	}

	public Map<String, Object> getFields()
	{
		return fields;
	}

	public void setFields(Map<String, Object> fields) 
	{
		this.fields = fields;
	}
	
}
