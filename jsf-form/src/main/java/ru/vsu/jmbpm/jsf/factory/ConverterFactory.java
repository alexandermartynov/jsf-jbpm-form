package ru.vsu.jmbpm.jsf.factory;

import javax.faces.convert.BigDecimalConverter;
import javax.faces.convert.BigIntegerConverter;
import javax.faces.convert.BooleanConverter;
import javax.faces.convert.ByteConverter;
import javax.faces.convert.CharacterConverter;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.DoubleConverter;
import javax.faces.convert.EnumConverter;
import javax.faces.convert.FloatConverter;
import javax.faces.convert.IntegerConverter;
import javax.faces.convert.LongConverter;
import javax.faces.convert.NumberConverter;
import javax.faces.convert.ShortConverter;

public class ConverterFactory 
{	
	private static final String BIG_DECIMAL = "BigDecimal"; 
	private static final String BIG_INTEGER = "BigInteger"; 
	private static final String BOOLEAN  = "Boolean"; 
	private static final String BYTE = "Byte"; 
	private static final String CHARACTER = "Character"; 
	private static final String DATE_TIME = "DateTime"; 
	private static final String DOUBLE = "Double"; 
	private static final String ENUM = "Enum"; 
	private static final String FLOAT = "Float"; 
	private static final String INTEGER = "Integer"; 
	private static final String LONG = "Long"; 
	private static final String NUMBER = "Number";
	private static final String SHORT = "Short";
	private static final String DATE = "Date";
	
	public static Converter getConvertor(Class<?> clazz)
	{			
		switch(clazz.getSimpleName())
		{
			case BIG_DECIMAL: return new BigDecimalConverter(); 
			case BIG_INTEGER: return new BigIntegerConverter(); 
			case BOOLEAN: return new BooleanConverter(); 
			case BYTE: return new ByteConverter(); 
			case CHARACTER: return new CharacterConverter(); 
			case DATE: return new DateTimeConverter();
			case DATE_TIME: return new DateTimeConverter(); 
			case DOUBLE: return new DoubleConverter(); 
			case ENUM: return new EnumConverter(); 
			case FLOAT: return new FloatConverter(); 
			case INTEGER: return new IntegerConverter(); 
			case LONG: return new LongConverter(); 
			case NUMBER: return new NumberConverter(); 
			case SHORT: return new ShortConverter(); 
			default: throw new ConverterException("Can't find convertor for class: ".concat(clazz.getName()));
		}
	}
}
