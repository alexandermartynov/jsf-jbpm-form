package ru.vsu.jmbpm.jsf.factory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ComponentsFactoryBuilder
{
	private static final String PROPERTIES_FILE = "jsf-jbpm-form.properties";
	private static final String PROPERTY_VALUE = "factory";
	private static ComponentsFactory componentsFactory = new JsfComponentsFactory();

	static 
	{
		InputStream input = ComponentsFactoryBuilder.class.getClassLoader()
				.getResourceAsStream(PROPERTIES_FILE);
		if (input != null) 
		{
			Properties prop = new Properties();
			try 
			{
				prop.load(input);
				String value = prop.getProperty(PROPERTY_VALUE);
				if(value != null)
				{
					Class<?> clazz = Class.forName(value);
					componentsFactory = (ComponentsFactory) clazz.newInstance();
				}			
			} 
			catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) 
			{
				e.printStackTrace();			
			} 
			finally 
			{
				if (input != null) 
				{
					try 
					{
						input.close();
					} 
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}

	public static ComponentsFactory getComponentsFactory() 
	{
		return componentsFactory;
	}
}
