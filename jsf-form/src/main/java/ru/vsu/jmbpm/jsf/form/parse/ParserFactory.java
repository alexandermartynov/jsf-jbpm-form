package ru.vsu.jmbpm.jsf.form.parse;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ParserFactory 
{
	private static final String PROPERTIES_FILE = "jsf-jbpm-form.properties";
	private static final String PROPERTY_FORM_VALUE = "parser.form";
	private static final String PROPERTY_FTL_VALUE = "parser.ftl";
	
	private static Parser formParser = new FormParser(); 
	private static Parser ftlParser = new FtlParser();
	
	static 
	{
		InputStream input = ParserFactory.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);
		if (input != null) 
		{
			Properties prop = new Properties();
			try 
			{
				prop.load(input);
				try
				{
					String value = prop.getProperty(PROPERTY_FORM_VALUE);
					if(value != null)
					{
						Class<?> clazz = Class.forName(value);
						formParser = (Parser) clazz.newInstance();
					}
				}
				catch(InstantiationException | IllegalAccessException e)
				{
					e.printStackTrace();					
				}
				try
				{
					String value = prop.getProperty(PROPERTY_FTL_VALUE);
					if(value != null)
					{
						Class<?> clazz = Class.forName(value);
						ftlParser = (Parser) clazz.newInstance();
					}
				}
				catch(InstantiationException | IllegalAccessException e)
				{
					e.printStackTrace();					
				}
			} 
			catch (IOException | ClassNotFoundException e) 
			{
				e.printStackTrace();				
			} 
			finally 
			{
				if (input != null) 
				{
					try 
					{
						input.close();
					} 
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public static Parser getFormParser()
	{
		return formParser;
	}
	
	public static Parser getFtlParser()
	{
		return ftlParser;
	}
}
