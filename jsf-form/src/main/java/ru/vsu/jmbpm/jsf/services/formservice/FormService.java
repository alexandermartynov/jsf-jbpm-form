package ru.vsu.jmbpm.jsf.services.formservice;

import java.util.Map;

import javax.ejb.Local;
import javax.faces.context.FacesContext;

import ru.vsu.jmbpm.jsf.form.Form;

@Local
public interface FormService 
{
	Form generateForm(FacesContext context,Map<String,Object> inputContext, String beanName, String name) throws Exception;
}
