package ru.vsu.jmbpm.jsf.form.parse;

public class FormulaParser 
{	
	private String beanName;
	
	public FormulaParser(String beanName)
	{
		this.beanName = beanName;
	}
	
	protected String getVariable(String name)
	{
		StringBuilder strb = new StringBuilder();
		strb.append(beanName)
			.append(".form.fields['")
			.append(name)
			.append("']");
		return strb.toString();
	}
	
	public String parse(String formula)
	{
		StringBuilder result = new StringBuilder();
		result.append("#{");
		for(int i = 1; i<formula.length(); i++) 
		{
			if(formula.charAt(i) == '{')
			{
				String name = "";
				for(i++, name+=formula.charAt(i), i++; i<formula.length() && formula.charAt(i) != '}'; i++)
				{
					name+=formula.charAt(i);
				}
				if(i == formula.length())
				{
					//TODO: ������� ����������, �� ������� ����������� ������
				} 
				else
				{
					result.append(getVariable(name));
				}				
			}
			else
			{
				result.append(formula.charAt(i));
			}
		}
		return result.append('}').toString();		
	}
}
