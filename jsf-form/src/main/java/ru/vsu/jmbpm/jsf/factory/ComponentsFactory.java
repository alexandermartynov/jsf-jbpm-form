package ru.vsu.jmbpm.jsf.factory;

import javax.faces.component.UIForm;
import javax.faces.component.UIInput;
import javax.faces.component.UIMessage;
import javax.faces.component.UIMessages;
import javax.faces.component.UIOutput;
import javax.faces.component.UIPanel;
import javax.faces.component.UICommand;
import javax.faces.component.UISelectOne;

public interface ComponentsFactory 
{
	UIForm getForm();
	
	UIInput getInputText();
	
	UIInput getInputTextaria();
	
	UIPanel getPanelGrid();
	
	UIOutput getOutputLabel();
	
	UICommand getCommandButton();
	
	UIMessages getMessages();
	
	UIMessage getMessage();
	
	UISelectOne getSelectOneListbox();
	
	UISelectOne getSelectOneMenu();
	
	UIInput getCalendar();
	
	UIOutput getOutputText();
}
