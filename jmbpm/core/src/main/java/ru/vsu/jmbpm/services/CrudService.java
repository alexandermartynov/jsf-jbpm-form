package ru.vsu.jmbpm.services;

import ru.vsu.jmbpm.dto.BaseDTO;

public interface CrudService<DTO extends BaseDTO> {

	DTO create(DTO dto);
	
	DTO read(Long id);
		
	DTO update(DTO dto);
	
	void delete(Long id);
		
}
