package ru.vsu.jmbpm.services;

import javax.ejb.Local;

import ru.vsu.jmbpm.dto.RoleDTO;
import ru.vsu.jmbpm.dto.UserDTO;
import ru.vsu.jmbpm.exception.JmbpmnException;

@Local
public interface UserService extends BaseServiceDTO<UserDTO> 
{
		
	UserDTO createUser(UserDTO userDTO) throws JmbpmnException;
	
	UserDTO createUser(UserDTO userDTO, RoleDTO role) throws JmbpmnException;
	
	UserDTO addRole(UserDTO userDTO, RoleDTO role);
	
	UserDTO getUserByLogin(String login);
		
}
