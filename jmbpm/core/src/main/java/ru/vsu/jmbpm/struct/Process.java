package ru.vsu.jmbpm.struct;

public class Process
{
	private String processId;

	public Process(String processId)
	{
		this.processId = processId;
	}
	
	public String getProcessId()
	{
		return processId;
	}

	public void setProcessId(String processId)
	{
		this.processId = processId;
	}

}
