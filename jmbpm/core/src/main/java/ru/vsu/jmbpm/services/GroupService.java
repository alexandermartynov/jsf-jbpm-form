package ru.vsu.jmbpm.services;

import java.util.List;

import javax.ejb.Local;

import ru.vsu.jmbpm.dto.GroupDTO;

@Local
public interface GroupService extends BaseServiceDTO<GroupDTO> {
		
	GroupDTO getGroupByName(String name);
	
	List<String> getUserGroupsName(String username);
		
}
