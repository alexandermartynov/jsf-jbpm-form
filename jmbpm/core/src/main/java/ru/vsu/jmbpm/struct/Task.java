package ru.vsu.jmbpm.struct;

import java.util.Map;

public class Task 
{
	private String name;
	private Long taskId;
	private String formName;
	private String description;
	private String owner;
	private Map<String, Object> inputContext;
	
	public Task()
	{
		
	}
	
	public Task(Long taskId, String name)
	{
		this.taskId = taskId;
		this.name = name;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public Long getTaskId() 
	{
		return taskId;
	}

	public void setTaskId(Long taskId)
	{
		this.taskId = taskId;
	}

	public String getFormName()
	{
		return formName;
	}

	public void setFormName(String formName) 
	{
		this.formName = formName;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getOwner() 
	{
		return owner;
	}

	public void setOwner(String owner) 
	{
		this.owner = owner;
	}

	public Map<String, Object> getInputContext()
	{
		return inputContext;
	}

	public void setInputContext(Map<String, Object> inputContext)
	{
		this.inputContext = inputContext;
	}
	
}
