package ru.vsu.jmbpm.services;

import java.io.File;

import javax.ejb.Local;

@Local
public interface DeploymentService {

	void deploy(File file);
	
}
