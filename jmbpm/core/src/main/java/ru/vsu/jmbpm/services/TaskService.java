package ru.vsu.jmbpm.services;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import ru.vsu.jmbpm.struct.Task;

@Local
public interface TaskService 
{
	List<Task> getTasksByPotentialOwner(String owner);
	
	List<Task> getCurrentTasksByOwner(String owner);
	
	Task getTask(Long taskId);
	
	void claimTask(Long taskId, String username);
	
	void startTask(Long taskId, String username);
	
	void stopTask(Long taskId, String username);
	
	void completeTask(Long taskId, String username, Map<String, Object> params);
	
}
