package ru.vsu.jmbpm.exception;

public class JmbpmnException extends Exception {
	private static final long serialVersionUID = 690425211931586048L;
	private String message;
	
	public JmbpmnException(){
		super();
	}
	
	public JmbpmnException(String message) {
		super(message);
		this.message = message;
	}
	
	public String toString() {
		return "JnbpmnException: " + message;
	}
}
