package ru.vsu.jmbpm.services;

import java.util.List;

import ru.vsu.jmbpm.dto.BaseDTO;

public interface BaseServiceDTO<DTO extends BaseDTO> extends CrudService<DTO> {

	List<DTO> readAll();
	
	List<DTO> getPage(int page, int count);
	
}
