package ru.vsu.jmbpm.dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USERS")
public class UserDTO extends BaseDTO
{
	private Long id;
	private String login;
	private String password;
	private Set<UserRolesDTO> usersRoles = new HashSet<UserRolesDTO>();
	private Set<UserGroupsDTO> usersGroups = new HashSet<UserGroupsDTO>(); 

	public UserDTO()
	{
		
	}
	
	public UserDTO(String login, String password)
	{
		this.login = login;
		this.password = password;
	}
	
	@Id
	@GeneratedValue
	@Column(name = "ID")
	@Override
	public Long getId()
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}
	
	@Column(name = "LOGIN")
	public String getLogin() 
	{
		return login;
	}
	
	public void setLogin(String login)
	{
		this.login = login;
	}
	
	@Column(name = "PASSWORD")
	public String getPassword()
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "role")
	public Set<UserRolesDTO> getUsersRoles() 
	{
		return usersRoles;
	}

	public void setUsersRoles(Set<UserRolesDTO> usersRoles)
	{
		this.usersRoles = usersRoles;
	}

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
	public Set<UserGroupsDTO> getUsersGroups() 
	{
		return usersGroups;
	}

	public void setUsersGroups(Set<UserGroupsDTO> usersGroups)
	{
		this.usersGroups = usersGroups;
	}

}
