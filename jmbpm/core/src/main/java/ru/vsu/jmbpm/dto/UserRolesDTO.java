package ru.vsu.jmbpm.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "USERS_ROLES")
public class UserRolesDTO {

	private RoleDTO role;
	private UserDTO user;
	private Long id;
	
	@Id
	@GeneratedValue
	@Column(name = "ID")
	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}
	
	@ManyToOne
    @JoinColumn(name = "ROLE_ID")
	public RoleDTO getRole() 
	{
		return role;
	}
	
	public void setRole(RoleDTO role) 
	{
		this.role = role;
	}
	
	@ManyToOne
    @JoinColumn(name = "USER_ID")
	public UserDTO getUser()
	{
		return user;
	}
	
	public void setUser(UserDTO user) 
	{
		this.user = user;
	}	
	
}
