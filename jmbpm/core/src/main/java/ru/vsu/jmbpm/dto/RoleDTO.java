package ru.vsu.jmbpm.dto;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ROLES")
public class RoleDTO extends BaseDTO 
{

	private Long id;
	private String name;	
	private Set<UserRolesDTO> usersRoles = new HashSet<UserRolesDTO>(); 

	@Id
	@GeneratedValue
	@Column(name = "ID")
	@Override
	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}
    
    @Column(name = "NAME")
	public String getName()
    {
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	@OneToMany(mappedBy = "role")
	public Set<UserRolesDTO> getUsersRoles() 
	{
		return usersRoles;
	}

	public void setUsersRoles(Set<UserRolesDTO> usersRoles)
	{
		this.usersRoles = usersRoles;
	}
	
}
