package ru.vsu.jmbpm.services;

import java.util.List;

import javax.ejb.Local;

import ru.vsu.jmbpm.struct.Process; 

@Local
public interface ProcessService 
{
	/**
	 * Запуск JBMP процесса по ИД процесса.
	 * @param processId - ИД процесса.
	 * @return
	 */
	Long startProcess(String processId); 
	
	/**
	 * Получить ИД всех процессов.
	 * @return
	 */
	List<Process> getAllProcessIds();
	
	/**
	 * Получить ИД всех активных процессов.
	 * @return
	 */
	List<Process> getAllActiveProcessIds();
}
