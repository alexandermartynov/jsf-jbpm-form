package ru.vsu.jmbpm.services;

import javax.ejb.Local;

import ru.vsu.jmbpm.dto.RoleDTO;

@Local
public interface RoleService extends BaseServiceDTO<RoleDTO> {

	RoleDTO getRoleByName(String name);
		
	Boolean isRoleContains(Long userId, String role); 
	
}
