package ru.vsu.jmbpm.jbpm;

import org.jbpm.kie.services.impl.KModuleDeploymentUnit;
import org.jbpm.services.api.DeploymentService;
import org.jbpm.services.api.model.DeploymentUnit;
import org.jbpm.services.cdi.Kjar;
import org.kie.internal.runtime.cdi.BootOnLoad;

import java.util.jar.JarFile;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
@BootOnLoad
public class DeploymentBean 
{
	public static final String DEPLOYMENT_ID = "ru.vsu:workflows:0.0.1-SNAPSHOT";

    @Inject
    @Kjar
    DeploymentService deploymentService;

    @PostConstruct
    public void init() 
    {
        String[] gav = DEPLOYMENT_ID.split(":");
        DeploymentUnit deploymentUnit = new KModuleDeploymentUnit(gav[0], gav[1], gav[2]);
        deploymentService.deploy(deploymentUnit);
    }
}
