package ru.vsu.jmbpm.jbpm;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import ru.vsu.jmbpm.services.GroupService;
import ru.vsu.jmbpm.services.UserService;

@Stateless
public class UserGroup implements UserGroupCallbackLocal {

	@EJB
	private UserService userService;
	
	@EJB
	private GroupService groupServise;
	
	public boolean existsUser(String userId) 
	{
        return userService.getUserByLogin(userId) != null;
    }

    public boolean existsGroup(String groupId) 
    {
        return groupServise.getGroupByName(groupId) != null;
    }

    public List<String> getGroupsForUser(String userId,
                                         List<String> groupIds, List<String> allExistingGroupIds)
                                         {    
        return groupServise.getUserGroupsName(userId);
    }

}
