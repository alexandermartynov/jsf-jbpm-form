package ru.vsu.jmbpm.services.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jbpm.services.api.RuntimeDataService;
import org.jbpm.services.api.UserTaskService;
import org.kie.api.task.model.Status;
import org.kie.api.task.model.TaskSummary;

import ru.vsu.jmbpm.services.GroupService;
import ru.vsu.jmbpm.struct.Task;
import org.kie.internal.task.api.model.InternalTask;

@Stateless
public class TaskLocalServiceImpl implements TaskLocalService
{
	@Inject
    private RuntimeDataService runtimeDataService;
	
	@Inject
	private UserTaskService userTaskService;
	
	@EJB
	private GroupService groupService;
	
	private Boolean checkStatus(Long taskId, Status status)
	{
		org.kie.api.task.model.Task task = userTaskService.getTask(taskId);
		return task.getTaskData().getStatus().equals(status);
	}
	
	@Override
	public List<Task> getTasksByPotentialOwner(String owner)
	{
		List<Task> result = new ArrayList<Task>();
		for(TaskSummary task : runtimeDataService.getTasksAssignedAsPotentialOwner(null, 
				groupService.getUserGroupsName(owner), null))
		{
			if(task.getActualOwnerId() == null) 
			{
				result.add(new Task(task.getId(), task.getName()));
			}			
		}
		return result;
	}
	
	@Override
	public List<Task> getCurrentTasksByOwner(String owner) 
	{
		List<Task> result = new ArrayList<Task>();
		
		for(TaskSummary task : runtimeDataService.getTasksOwned(owner, null))
		{
			result.add(new Task(task.getId(), task.getName()));
		}
		return result;
	}
	
	@Override
	public Task getTask(Long taskId)
	{
		org.kie.api.task.model.Task task = userTaskService.getTask(taskId);
		Task result = new Task(task.getId(), task.getName());
		result.setDescription(task.getDescription());
		result.setFormName(((InternalTask)task).getFormName());
		result.setOwner(task.getTaskData().getActualOwner() == null ? "" 
				:task.getTaskData().getActualOwner().getId());
		result.setInputContext(userTaskService.getTaskInputContentByTaskId(taskId));
		return result;
	}

	@Override
	public void claimTask(Long taskId, String username) 
	{
		if(checkStatus(taskId, Status.Ready))
		{
			userTaskService.claim(taskId, username);	
		}
	}

	@Override
	public void completeTask(Long taskId, String username,
			Map<String, Object> params)
	{
		if(checkStatus(taskId, Status.InProgress))
		{
			userTaskService.complete(taskId, username, params);		
		}
	}

	@Override
	public void startTask(Long taskId, String username)
	{
		if(checkStatus(taskId, Status.Reserved))
		{
			userTaskService.start(taskId, username);
		}
	}

	@Override
	public void stopTask(Long taskId, String username)
	{
		if(checkStatus(taskId, Status.InProgress))
		{
			userTaskService.stop(taskId, username);
		}
	}
}
