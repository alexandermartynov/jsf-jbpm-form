package ru.vsu.jmbpm.start;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import ru.vsu.jmbpm.dto.GroupDTO;
import ru.vsu.jmbpm.dto.RoleDTO;
import ru.vsu.jmbpm.dto.UserDTO;
import ru.vsu.jmbpm.exception.JmbpmnException;
import ru.vsu.jmbpm.services.GroupService;
import ru.vsu.jmbpm.services.RoleService;
import ru.vsu.jmbpm.services.UserService;

@Startup
@Singleton
public class StartupBean {
	
	@EJB
	private RoleService roleService;
	
	@EJB
	private GroupService groupService;
	
	@EJB
	private UserService userService;
	
	@PostConstruct
	void init()
	{
		if(groupService.getGroupByName("ALL") == null)
		{
			GroupDTO groupe = new GroupDTO();
			groupe.setName("ALL");
			groupService.create(groupe);
		}
		if(roleService.getRoleByName("USER") == null) 
		{
			RoleDTO role = new RoleDTO();
			role.setName("USER");
			roleService.create(role);
		}
		RoleDTO roleAdmin = roleService.getRoleByName("ADMIN");
		if(roleAdmin == null) 
		{
			roleAdmin = new RoleDTO();
			roleAdmin.setName("ADMIN");
			roleAdmin = roleService.create(roleAdmin);
		}
		if(userService.getUserByLogin("Administrator") == null)
		{
			try 
			{
				UserDTO admin = new UserDTO("Administrator", "Administrator");
				userService.createUser(admin, roleAdmin);
			} catch (JmbpmnException e)
			{
				e.printStackTrace();
			}
		}
	}
}
