package ru.vsu.jmbpm.services.base;

import java.util.List;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import ru.vsu.jmbpm.dto.BaseDTO;
import ru.vsu.jmbpm.services.BaseServiceDTO;

@TransactionManagement(TransactionManagementType.CONTAINER)
public abstract class BaseService<DTO extends BaseDTO> implements BaseServiceDTO<DTO> {

	public abstract BaseServiceDTO<DTO> getLocalService();
	
	@Override
	public DTO create(DTO dto)
	{
		return getLocalService().create(dto);
	}

	@Override
	public DTO read(Long id) 
	{
		return getLocalService().read(id);
	}

	@Override
	public DTO update(DTO dto) 
	{
		return getLocalService().update(dto);
	}

	@Override
	public void delete(Long id) 
	{
		getLocalService().delete(id);
	}

	@Override
	public List<DTO> readAll() 
	{
		return getLocalService().readAll();
	}

	@Override
	public List<DTO> getPage(int page, int count) 
	{
		return getLocalService().getPage(page, count);
	}
	
}
