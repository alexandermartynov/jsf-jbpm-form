package ru.vsu.jmbpm.services.role;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import ru.vsu.jmbpm.dto.RoleDTO;
import ru.vsu.jmbpm.services.BaseServiceDTO;
import ru.vsu.jmbpm.services.RoleService;
import ru.vsu.jmbpm.services.base.BaseService;

@Stateless
@TransactionAttribute(value=TransactionAttributeType.REQUIRED)
@TransactionManagement(value=TransactionManagementType.CONTAINER)
public class RoleServiceImpl extends BaseService<RoleDTO> implements RoleService {

	@EJB
	private RoleLocalService rolesLocalService;
	
	@Override
	public RoleDTO getRoleByName(String name) 
	{
		return rolesLocalService.getRoleByName(name);
	}

	@Override
	public Boolean isRoleContains(Long userId, String role)
	{
		return rolesLocalService.isRoleContains(userId, role);
	}

	@Override
	public BaseServiceDTO<RoleDTO> getLocalService() 
	{
		return rolesLocalService;
	}
	
}
