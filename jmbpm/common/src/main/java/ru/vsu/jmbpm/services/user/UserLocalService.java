package ru.vsu.jmbpm.services.user;

import javax.ejb.Local;

import ru.vsu.jmbpm.services.UserService;

@Local
public interface UserLocalService extends UserService
{

}
