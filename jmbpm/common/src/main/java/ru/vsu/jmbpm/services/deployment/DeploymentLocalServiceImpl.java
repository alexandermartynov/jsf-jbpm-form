package ru.vsu.jmbpm.services.deployment;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.jbpm.kie.services.impl.KModuleDeploymentUnit;
import org.jbpm.services.api.DeploymentService;
import org.jbpm.services.cdi.Kjar;

import ru.vsu.jmbpm.utils.MavenUtils;

@Stateless
public class DeploymentLocalServiceImpl implements DeploymentLocalService {

	private static final String VERSION = "version";
	private static final String GROUP_ID = "groupId";
	private static final String ARTIFACT_ID = "artifactId";
	private static final String FILE = "pom.properties";
	
	@Inject
    @Kjar
    DeploymentService deploymentService;
		
	@Override
	public void deploy(File file) {			
		try {
			JarFile jarFile = new JarFile(file);
			Enumeration<JarEntry> entries = jarFile.entries();
			while(entries.hasMoreElements()){
				JarEntry entry = entries.nextElement();
				if(entry.getName().indexOf(FILE) > 0 ){
					Properties pomProperty = new Properties();
					pomProperty.load(jarFile.getInputStream(entry));
					String localRepository = MavenUtils.getLocalRepository();
					String groupId = pomProperty.getProperty(GROUP_ID).replaceAll("\\.", Matcher.quoteReplacement(File.separator));
					String artifactId = pomProperty.getProperty(ARTIFACT_ID).replaceAll("\\.", Matcher.quoteReplacement(File.separator));
					String version = pomProperty.getProperty(VERSION);
					File f = new File(localRepository+
							File.separator+pomProperty.getProperty(GROUP_ID).replaceAll("\\.", Matcher.quoteReplacement(File.separator))+
							File.separator+pomProperty.getProperty(ARTIFACT_ID).replaceAll("\\.", Matcher.quoteReplacement(File.separator))+
							File.separator+pomProperty.getProperty(VERSION)+
							File.separator+file.getName());
					f.getParentFile().mkdirs(); 
					FileUtils.copyFile(file, f);
					deploymentService.deploy(new KModuleDeploymentUnit(groupId, artifactId, version));
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
