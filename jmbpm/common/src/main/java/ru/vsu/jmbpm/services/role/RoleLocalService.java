package ru.vsu.jmbpm.services.role;

import javax.ejb.Local;

import ru.vsu.jmbpm.services.RoleService;

@Local
public interface RoleLocalService extends RoleService
{
	
}
