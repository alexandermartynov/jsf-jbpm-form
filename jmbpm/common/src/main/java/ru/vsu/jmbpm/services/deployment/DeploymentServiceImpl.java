package ru.vsu.jmbpm.services.deployment;

import java.io.File;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import ru.vsu.jmbpm.services.DeploymentService;

@Stateless
public class DeploymentServiceImpl implements DeploymentService {

	@EJB
	private DeploymentLocalService localService;
	
	@Override
	public void deploy(File file) {
		localService.deploy(file);
	}

}
