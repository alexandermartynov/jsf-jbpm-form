package ru.vsu.jmbpm.services.process;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import ru.vsu.jmbpm.services.ProcessService;
import ru.vsu.jmbpm.struct.Process; 

@Stateless
public class ProcessServiceImpl implements ProcessService
{

	@EJB
	private ProcessLocalService processLocalService;
	
	/**
	 * Запуск JBMP процесса по ИД процесса.
	 * @param processId - ИД процесса.
	 * @return
	 */
	@Override
	public Long startProcess(String processId) 
	{
		return processLocalService.startProcess(processId, null);
	}
	
	@Override
	public List<Process> getAllProcessIds()
	{
		return processLocalService.getAllProcessIds();
	}
	
	@Override
	public List<Process> getAllActiveProcessIds()
	{
		return processLocalService.getAllActiveProcessIds();
	}
}
