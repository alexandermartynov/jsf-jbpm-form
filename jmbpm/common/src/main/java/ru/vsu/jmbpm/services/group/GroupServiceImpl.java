package ru.vsu.jmbpm.services.group;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import ru.vsu.jmbpm.dto.GroupDTO;
import ru.vsu.jmbpm.services.BaseServiceDTO;
import ru.vsu.jmbpm.services.GroupService;
import ru.vsu.jmbpm.services.base.BaseService;

@Stateless
@TransactionAttribute(value=TransactionAttributeType.REQUIRED)
@TransactionManagement(value=TransactionManagementType.CONTAINER)
public class GroupServiceImpl extends BaseService<GroupDTO> implements GroupService
{

	@EJB 
	private GroupLocalService groupLocalService;
	
	@Override
	public GroupDTO getGroupByName(String name)
	{
		return groupLocalService.getGroupByName(name);
	}

	@Override
	public List<String> getUserGroupsName(String username)
	{
		return groupLocalService.getUserGroupsName(username);
	}

	@Override
	public BaseServiceDTO<GroupDTO> getLocalService() {
		return groupLocalService;
	}

}
