package ru.vsu.jmbpm.jbpm;

import javax.ejb.Local;

import org.kie.api.task.UserGroupCallback;

@Local
public interface UserGroupCallbackLocal extends UserGroupCallback {

}
