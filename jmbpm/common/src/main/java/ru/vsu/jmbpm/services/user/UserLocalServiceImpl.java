package ru.vsu.jmbpm.services.user;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import ru.vsu.jmbpm.dto.GroupDTO;
import ru.vsu.jmbpm.dto.RoleDTO;
import ru.vsu.jmbpm.dto.UserDTO;
import ru.vsu.jmbpm.dto.UserGroupsDTO;
import ru.vsu.jmbpm.dto.UserRolesDTO;
import ru.vsu.jmbpm.exception.JmbpmnException;
import ru.vsu.jmbpm.services.GroupService;
import ru.vsu.jmbpm.services.RoleService;
import ru.vsu.jmbpm.services.base.BaseLocalService;

@Stateless
@TransactionAttribute(value=TransactionAttributeType.REQUIRED)
@TransactionManagement(value=TransactionManagementType.CONTAINER)
public class UserLocalServiceImpl extends BaseLocalService<UserDTO> implements UserLocalService {
    
	public UserLocalServiceImpl() 
	{
		super(UserDTO.class);
	}

	private static final String DEFAULT_ROLE = "USER";
	private static final String DEFAULT_GROUPE = "ALL";
	
	@EJB
	private RoleService roleService;
	
	@EJB
	private GroupService groupService;
	
	@Override
	public UserDTO createUser(UserDTO dto) throws JmbpmnException
	{
		if(getUserByLogin(dto.getLogin()) != null) {
			throw new JmbpmnException("Пользователь с таким логином уже существует");
		}
		dto = create(dto);
		RoleDTO role = roleService.getRoleByName(DEFAULT_ROLE);
		GroupDTO group = groupService.getGroupByName(DEFAULT_GROUPE);
		UserGroupsDTO usersGroups = new UserGroupsDTO();
		usersGroups.setGroup(group);
		usersGroups.setUser(dto);
		usersGroups = getEntityManager().merge(usersGroups);
		UserRolesDTO usersRoles = new UserRolesDTO();
		usersRoles.setRole(role);
		usersRoles.setUser(dto);
		usersRoles = getEntityManager().merge(usersRoles);
		dto.getUsersGroups().add(usersGroups);
		dto.getUsersRoles().add(usersRoles);
		group.getUsersGroups().add(usersGroups);
		role.getUsersRoles().add(usersRoles);
		getEntityManager().merge(group);
		getEntityManager().merge(role);
		return getEntityManager().merge(dto);
	}

	@Override
	public UserDTO getUserByLogin(String login) 
	{
		List<UserDTO> users =  getEntityManager()
				.createQuery("select u from UserDTO as u where u.login = :login")
				.setParameter("login", login)
				.getResultList();
		if(users.size() != 1 )
		{
			return null;
		}
		return users.get(0);
	}

	@Override
	public UserDTO createUser(UserDTO userDTO, RoleDTO role) throws JmbpmnException
	{
		userDTO = createUser(userDTO);
		return addRole(userDTO, role);
	}
	
	@Override
	public UserDTO addRole(UserDTO userDTO, RoleDTO role)
	{
		UserRolesDTO usersRoles = new UserRolesDTO();
		usersRoles.setRole(role);
		usersRoles.setUser(userDTO);
		usersRoles = getEntityManager().merge(usersRoles);
		role.getUsersRoles().add(usersRoles);
		getEntityManager().merge(role);
		userDTO.getUsersRoles().add(usersRoles);
		return getEntityManager().merge(userDTO);
	}
}
