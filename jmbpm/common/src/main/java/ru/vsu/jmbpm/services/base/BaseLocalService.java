package ru.vsu.jmbpm.services.base;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import ru.vsu.jmbpm.dto.BaseDTO;
import ru.vsu.jmbpm.services.BaseServiceDTO;

public abstract class BaseLocalService<DTO extends BaseDTO> implements BaseServiceDTO<DTO> {

	@PersistenceUnit(unitName = "org.jbpm.domain")
    private EntityManagerFactory emf;

	private EntityManager em;

	private Class<DTO> instance;

	public BaseLocalService(Class<DTO> instance) {
		this.instance = instance;
	}
	    
    protected EntityManager getEntityManager()
    {
    	if(em == null) 
    	{
    		em = emf.createEntityManager();
    	}
        return em;
    }
    
    @Override
    public DTO create(DTO dto)
    {
    	return getEntityManager().merge(dto);
    }
	
    @Override
	public DTO read(Long id)
	{
		return getEntityManager().find(instance, id);
	}
		
    @Override
	public DTO update(DTO dto) 
	{
		return getEntityManager().merge(dto);
	}
	
    @Override
	public void delete(Long id) 
	{
		getEntityManager().remove(read(id));
	}
	
    protected TypedQuery<DTO> getTypedQueryDTO()
    {
    	CriteriaQuery<DTO> cq = getEntityManager().getCriteriaBuilder()
        		.createQuery(instance);
        Root<DTO> rootEntry = cq.from(instance);
        CriteriaQuery<DTO> all = cq.select(rootEntry);
        return getEntityManager().createQuery(all);
    }
    
    @Override
	public List<DTO> readAll()
	{
        return getTypedQueryDTO().getResultList();
	}
    
    @Override
    public List<DTO> getPage(int page, int count)
    {
    	return getTypedQueryDTO().setFirstResult((page-1)*count)
    			.setMaxResults(count).getResultList();
    }
}
