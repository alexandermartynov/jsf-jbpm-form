package ru.vsu.jmbpm.services.group;

import javax.ejb.Local;

import ru.vsu.jmbpm.services.GroupService;

@Local
public interface GroupLocalService extends GroupService 
{
	
}
