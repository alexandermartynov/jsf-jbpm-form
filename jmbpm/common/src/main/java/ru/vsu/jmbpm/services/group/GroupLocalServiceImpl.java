package ru.vsu.jmbpm.services.group;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import ru.vsu.jmbpm.dto.GroupDTO;
import ru.vsu.jmbpm.services.base.BaseLocalService;

@Stateless
@TransactionAttribute(value=TransactionAttributeType.REQUIRED)
@TransactionManagement(value=TransactionManagementType.CONTAINER)
public class GroupLocalServiceImpl extends BaseLocalService<GroupDTO> implements GroupLocalService
{

	public GroupLocalServiceImpl() 
	{
		super(GroupDTO.class);
	}

	@Override
	public GroupDTO getGroupByName(String name) {
		List<GroupDTO> groups =  getEntityManager()
				.createQuery("select g from GroupDTO as g where g.name = :name")
				.setParameter("name", name)
				.getResultList();
		if(groups.size() != 1 )
		{
			return null;
		}
		return groups.get(0);
	}

	@Override
	public List<String> getUserGroupsName(String username) {
		StringBuilder strb = new StringBuilder();
		strb.append("select g.group.name from UserGroupsDTO as g ")
			.append("inner join g.user as u ")
			.append("where u.login = :login ");
		return getEntityManager()
				.createQuery(strb.toString())
				.setParameter("login", username)
				.getResultList();
	}

}
