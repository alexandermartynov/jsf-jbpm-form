package ru.vsu.jmbpm.services.process;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import ru.vsu.jmbpm.struct.Process; 

@Local
public interface ProcessLocalService 
{
	/**
	 * Запуск JBMP процесса по ИД процесса с параметрами.
	 * @param processId - ИД процесса.
	 * @param params - параметры.
	 * @return
	 */
	Long startProcess(String processId, Map<String, Object> params);
	
	List<Process> getAllProcessIds();
	
	List<Process> getAllActiveProcessIds();
}
