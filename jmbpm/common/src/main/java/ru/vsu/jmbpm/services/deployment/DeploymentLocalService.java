package ru.vsu.jmbpm.services.deployment;

import javax.ejb.Local;

import ru.vsu.jmbpm.services.DeploymentService;

@Local
public interface DeploymentLocalService extends DeploymentService {

}
