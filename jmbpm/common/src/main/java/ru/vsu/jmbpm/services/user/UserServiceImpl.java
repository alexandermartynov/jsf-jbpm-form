package ru.vsu.jmbpm.services.user;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import ru.vsu.jmbpm.dto.RoleDTO;
import ru.vsu.jmbpm.dto.UserDTO;
import ru.vsu.jmbpm.exception.JmbpmnException;
import ru.vsu.jmbpm.services.BaseServiceDTO;
import ru.vsu.jmbpm.services.UserService;
import ru.vsu.jmbpm.services.base.BaseService;

@Stateless
@TransactionAttribute(value=TransactionAttributeType.REQUIRED)
@TransactionManagement(value=TransactionManagementType.CONTAINER)
public class UserServiceImpl extends BaseService<UserDTO> implements UserService
{

	@EJB
	private UserLocalService userLocalService;
	
	@Override
	public UserDTO getUserByLogin(String login) 
	{
		return userLocalService.getUserByLogin(login);
	}

	@Override
	public UserDTO addRole(UserDTO userDTO, RoleDTO role) 
	{
		return userLocalService.addRole(userDTO, role);
	}

	@Override
	public UserDTO createUser(UserDTO userDTO, RoleDTO role) throws JmbpmnException
	{
		return userLocalService.createUser(userDTO, role);
	}

	@Override
	public UserDTO createUser(UserDTO userDTO) throws JmbpmnException {
		return userLocalService.createUser(userDTO);
	}
	

	@Override
	public BaseServiceDTO<UserDTO> getLocalService() 
	{
		return userLocalService;
	}
}
