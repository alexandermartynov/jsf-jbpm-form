package ru.vsu.jmbpm.services.process;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.jbpm.services.api.ProcessService;
import org.jbpm.services.api.RuntimeDataService;
import org.jbpm.services.api.model.ProcessDefinition;
import org.jbpm.services.api.model.ProcessInstanceDesc;
import org.kie.api.runtime.process.ProcessInstance;

import ru.vsu.jmbpm.struct.Process; 

@Stateless
public class ProccesLocalServiceImpl implements ProcessLocalService
{
	private static final String DEPLOYMENT_ID = "ru.vsu:workflows:0.0.1-SNAPSHOT";
	
	@Inject
    private ProcessService processService;
	
	@Inject
	private RuntimeDataService runtimeDataService;
	
	@Override
	public Long startProcess(String processId, Map<String, Object> params) 
	{
		if(params == null)
		{
			return processService.startProcess(DEPLOYMENT_ID, processId, params);
		}
		return processService.startProcess(DEPLOYMENT_ID, processId ); 
	}
	
	@Override
	public List<Process> getAllProcessIds()
	{
		List<Process> result = new ArrayList<Process>();
		for(ProcessDefinition processDefinition : runtimeDataService.getProcessesByDeploymentId(DEPLOYMENT_ID, null))
		{
			result.add(new Process(processDefinition.getId()));
		}
		return result;
	}
	
	@Override
	public List<Process> getAllActiveProcessIds()
	{
		List<Process> result = new ArrayList<Process>();
		List<Integer> states = new ArrayList<Integer>();
		states.add(ProcessInstance.STATE_ACTIVE);
		for(ProcessInstanceDesc processDefinition : runtimeDataService.getProcessInstancesByDeploymentId(DEPLOYMENT_ID, states, null))
		{
			result.add(new Process(processDefinition.getProcessId()));
		}
		return result;		
	}

}
