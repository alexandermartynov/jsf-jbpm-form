package ru.vsu.jmbpm.jbpm;

import org.kie.internal.identity.IdentityProvider;

import java.util.ArrayList;
import java.util.List;

public class Identity implements IdentityProvider 
{

	private List<String> roles = new ArrayList<String>();

    @Override
    public String getName() 
    {
        return "system";
    }

    @Override
    public List<String> getRoles()
    {
        return roles;
    }

    @Override
    public boolean hasRole(String s) 
    {
        return true;
    }
}
