package ru.vsu.jmbpm.services.role;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import ru.vsu.jmbpm.dto.RoleDTO;
import ru.vsu.jmbpm.services.base.BaseLocalService;

@Stateless
@TransactionAttribute(value=TransactionAttributeType.REQUIRED)
@TransactionManagement(value=TransactionManagementType.CONTAINER)
public class RoleLocalServiceImpl extends BaseLocalService<RoleDTO> implements RoleLocalService {

	public RoleLocalServiceImpl()
	{
		super(RoleDTO.class);
	}

	@Override
	public RoleDTO getRoleByName(String name) {
		List<RoleDTO> roles =  getEntityManager()
				.createQuery("select r from RoleDTO as r where r.name = :name")
				.setParameter("name", name)
				.getResultList();
		if(roles.size() != 1 )
		{
			return null;
		}
		return roles.get(0);
	}

	@Override
	public Boolean isRoleContains(Long userId, String role)
	{
		StringBuilder strb = new StringBuilder();
		strb.append("select ur from UserRolesDTO ur ")
			.append("inner join ur.user u ")
			.append("inner join ur.role r ")
			.append("where u.id = :userId ")
			.append("and r.name = :role");
		return getEntityManager()
				.createQuery(strb.toString())
				.setParameter("userId", userId)
				.setParameter("role", role)
				.getResultList()
				.size() == 1;
	}

}
