package ru.vsu.jmbpm.services.task;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import ru.vsu.jmbpm.services.TaskService;
import ru.vsu.jmbpm.struct.Task;

@Stateless
public class TaskServiceImpl implements TaskService
{
	@EJB
	private TaskLocalService taskLocalService;
	
	@Override
	public List<Task> getTasksByPotentialOwner(String owner)
	{
		return taskLocalService.getTasksByPotentialOwner(owner);
	}
	
	public Task getTask(Long taskId)
	{
		return taskLocalService.getTask(taskId);
	}

	@Override
	public List<Task> getCurrentTasksByOwner(String owner) {
		return taskLocalService.getCurrentTasksByOwner(owner);
	}

	@Override
	public void completeTask(Long taskId, String username,
			Map<String, Object> params) 
	{
		taskLocalService.completeTask(taskId, username, params);		
	}

	@Override
	public void claimTask(Long taskId, String username) 
	{
		taskLocalService.claimTask(taskId, username);
	}

	@Override
	public void startTask(Long taskId, String username)
	{
		taskLocalService.startTask(taskId, username);
	}

	@Override
	public void stopTask(Long taskId, String username)
	{
		taskLocalService.stopTask(taskId, username);
	}
}
