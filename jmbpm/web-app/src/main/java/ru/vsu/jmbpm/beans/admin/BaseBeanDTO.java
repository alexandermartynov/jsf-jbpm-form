package ru.vsu.jmbpm.beans.admin;

import java.util.List;

import org.primefaces.event.SelectEvent;
import org.primefaces.mobile.event.SwipeEvent;

import ru.vsu.jmbpm.dto.BaseDTO;
import ru.vsu.jmbpm.services.BaseServiceDTO;

public abstract class BaseBeanDTO<DTO extends BaseDTO> 
{
	protected abstract BaseServiceDTO<DTO> getBaseService();
	public abstract DTO getDTO();
	public abstract void setDTO(DTO dto);
	
	public DTO save()
	{
		DTO dto = getDTO();
		if(dto.getId() == null)
		{
			setDTO(null);
			return getBaseService().create(dto);
		} else {
			return getBaseService().update(dto);
		}
	}
	
	public List<DTO> getPage()
	{
		return getBaseService().readAll();
	}
	
	@SuppressWarnings("unchecked")
	public void remove(SwipeEvent event)
	{
		DTO dto = (DTO)event.getData();
		getBaseService().delete(dto.getId());
	}
	
	@SuppressWarnings("unchecked")
	public void tap(SelectEvent event) {
		setDTO(getBaseService().read(((DTO)event.getObject()).getId()));
	}
	
}
