package ru.vsu.jmbpm.beans.form;

import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import ru.vsu.jmbpm.beans.user.UserBean;
import ru.vsu.jmbpm.jsf.form.Form;
import ru.vsu.jmbpm.jsf.services.formservice.FormService;
import ru.vsu.jmbpm.services.TaskService;
import ru.vsu.jmbpm.struct.Task;

@ManagedBean
@ViewScoped
public class FormBean {

	@EJB 
	private FormService formService;
	
	@EJB
	private TaskService taskService;
	
	private Form form = new Form();
	private Long taskId = null;
	private Task task = null;

	
	@ManagedProperty(value="#{userBean}")
	private UserBean userBean;

	public void setUserBean(UserBean userBean) 
	{
		this.userBean = userBean;
	}
	
	public UserBean getUserBean()
	{
		return userBean;
	}
	
	public Form getForm()
	{
		return form;
	}

	public void setForm(Form form)
	{
		this.form = form;
	}

	public void goToTaskForm() throws Exception 
	{
		Map<String, String[]> paramValues = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterValuesMap();
		taskId = Long.valueOf(paramValues.get("taskId")[0]).longValue();
		task = taskService.getTask(taskId);
		setForm(formService.generateForm(FacesContext.getCurrentInstance(), task.getInputContext()
				, "formBean", task.getFormName()));	
		taskService.startTask(taskId, userBean.getUsername());
	}
	

	public void action()
    {
		taskService.completeTask(taskId, userBean.getUsername(), form.getFields());
		RequestContext.getCurrentInstance().execute("window.location.replace('/')");
    }
	
	public void goToBack() 
	{
		taskService.stopTask(taskId, userBean.getUsername());
	}
	
}
