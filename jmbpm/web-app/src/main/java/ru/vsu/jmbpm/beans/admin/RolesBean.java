package ru.vsu.jmbpm.beans.admin;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ru.vsu.jmbpm.dto.RoleDTO;
import ru.vsu.jmbpm.services.BaseServiceDTO;
import ru.vsu.jmbpm.services.RoleService;

@ManagedBean
@ViewScoped
public class RolesBean extends BaseBeanDTO<RoleDTO> {

	@EJB
	private RoleService roleService;
	
	private RoleDTO dto;
	
	@Override
	protected BaseServiceDTO<RoleDTO> getBaseService() {
		return roleService;
	}

	@Override
	public RoleDTO getDTO() {
		if(dto == null)
			return new RoleDTO();
		return dto;
	}

	@Override
	public void setDTO(RoleDTO dto) {
		this.dto = dto;		
	}

}
