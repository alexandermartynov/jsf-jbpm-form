package ru.vsu.jmbpm.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.TabChangeEvent;

import ru.vsu.jmbpm.services.ProcessService;
import ru.vsu.jmbpm.struct.Process; 

@ManagedBean
@ViewScoped
public class ProcessBean
{
	@EJB
	private ProcessService processService;
	
	private List<Process> processes;
	
	private Process selectedProcess;
	
	@PostConstruct
	public void init()
	{
		processes = processService.getAllProcessIds();
	}

	public List<Process> getProcesses()
	{
		return processes;
	}

	public Process getSelectedProcess() 
	{
		return selectedProcess;
	}

	public void setSelectedProcess(Process selectedProcess) 
	{
		this.selectedProcess = selectedProcess;
	}	
	
	public void processSelectedAction()
	{
		processService.startProcess(getSelectedProcess().getProcessId());
	}
	
	public void onTabChange(TabChangeEvent event) 
	{
		if( event.getTab().getTitle().equals("Процессы"))
		{
			processes = processService.getAllProcessIds();
		}
		else
		{
			processes = processService.getAllActiveProcessIds();
		}
	}
	
}
