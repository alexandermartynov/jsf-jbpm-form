package ru.vsu.jmbpm.beans.task;

import java.util.Map;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import ru.vsu.jmbpm.beans.user.UserBean;
import ru.vsu.jmbpm.services.TaskService;
import ru.vsu.jmbpm.struct.Task;

@ManagedBean
@ViewScoped
public class TaskBean 
{
	@EJB
	private TaskService taskService;
	
	@ManagedProperty(value="#{userBean}")
	private UserBean userBean;

	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}
	
	public UserBean getUserBean()
	{
		return userBean;
	}
	
	private Task task;
	
	public Task getTask() 
	{
		return task;
	}

	public void setTask(Task task)
	{
		this.task = task;
	}

	public void refreshTask() {
		task = taskService.getTask(task.getTaskId());
	}
	
	public void claimTask()
	{
		taskService.claimTask(task.getTaskId(), userBean.getUsername());
	}
	
	public void startTask()
	{
		taskService.startTask(task.getTaskId(), userBean.getUsername());
	}
	
	public void stopTask()
	{
		taskService.stopTask(task.getTaskId(), userBean.getUsername());
	}
	
	public void goToTaskView(){
		Map<String, String[]> paramValues = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterValuesMap();
		task = taskService.getTask(Long.valueOf(paramValues.get("taskId")[0]).longValue());
	}
}
