package ru.vsu.jmbpm.beans.user;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import ru.vsu.jmbpm.dto.UserDTO;
import ru.vsu.jmbpm.exception.JmbpmnException;
import ru.vsu.jmbpm.services.UserService;

@ManagedBean
@ViewScoped
public class SignInBean 
{
	@EJB
	private UserService userService;
	
	private String username; 
	private String password;
	
	@ManagedProperty(value="#{userBean}")
	private UserBean userBean;

	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}
	
	public UserBean getUserBean()
	{
		return userBean;
	}
	
	public String getUsername() 
	{
		return username;
	}
	
	public void setUsername(String username) 
	{
		this.username = username;
	}
	
	public String getPassword() 
	{
		return password;
	}
	
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	public void signIn() 
	{
		try 
		{
			UserDTO user = userService.createUser(new UserDTO(username, password));
			userBean.setUser(user);			
	        RequestContext.getCurrentInstance().execute("window.location.replace('/')");
		} 
		catch (JmbpmnException e) 
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ошибка регистрации!", e.getMessage()));
		}
	}
}
