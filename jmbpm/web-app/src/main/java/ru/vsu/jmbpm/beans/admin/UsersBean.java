package ru.vsu.jmbpm.beans.admin;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ru.vsu.jmbpm.dto.UserDTO;
import ru.vsu.jmbpm.services.BaseServiceDTO;
import ru.vsu.jmbpm.services.UserService;

@ManagedBean
@ViewScoped
public class UsersBean extends BaseBeanDTO<UserDTO> {

	@EJB
	private UserService userService;
	
	private UserDTO dto;
	
	@Override
	protected BaseServiceDTO<UserDTO> getBaseService() {
		return userService;
	}

	@Override
	public UserDTO getDTO() {
		if(dto == null)
			return new UserDTO();
		return dto;
	}

	@Override
	public void setDTO(UserDTO dto) {
		this.dto = dto;
	}

}
