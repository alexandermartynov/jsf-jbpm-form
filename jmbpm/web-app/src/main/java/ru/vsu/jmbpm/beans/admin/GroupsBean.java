package ru.vsu.jmbpm.beans.admin;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ru.vsu.jmbpm.dto.GroupDTO;
import ru.vsu.jmbpm.services.BaseServiceDTO;
import ru.vsu.jmbpm.services.GroupService;

@ManagedBean
@ViewScoped
public class GroupsBean extends BaseBeanDTO<GroupDTO> {

	@EJB
	private GroupService groupService;
	
	private GroupDTO dto;
	
	@Override
	protected BaseServiceDTO<GroupDTO> getBaseService() {
		return groupService;
	}

	@Override
	public GroupDTO getDTO() {
		if(dto == null)
			return new GroupDTO();
		return dto;
	}

	@Override
	public void setDTO(GroupDTO dto) {
		this.dto = dto;
	}

}
