package ru.vsu.jmbpm.beans.user;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ru.vsu.jmbpm.dto.UserDTO;
import ru.vsu.jmbpm.services.RoleService;
import ru.vsu.jmbpm.services.UserService;

@ManagedBean
@SessionScoped
public class UserBean 
{	
	@EJB
	private UserService userService;
	
	@EJB
	private RoleService roleService;
	
	private UserDTO user = null;
	private Boolean superUser = false;
	public void logOut()
	{
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
	}
	
	public String getUsername()
	{
		return user == null ? "" : user.getLogin();
	}
	
	public UserDTO getUser()
	{
		return user;
	}

	public void setUser(UserDTO user)
	{
		this.user = user;
	}
	
	public void setUser(String login)
	{
		this.user = userService.getUserByLogin(login);
		this.superUser = roleService.isRoleContains(user.getId(), "ADMIN");
	}
	
	public Boolean isSuperUser() 
	{
		return superUser;			
	}
}
