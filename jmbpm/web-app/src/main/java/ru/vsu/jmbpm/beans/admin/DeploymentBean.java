package ru.vsu.jmbpm.beans.admin;

import java.io.File;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.UploadedFile;

import ru.vsu.jmbpm.services.DeploymentService;

@ManagedBean
@ViewScoped
public class DeploymentBean {

	@EJB
	private DeploymentService service;

	private UploadedFile file;

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public void upload() throws Exception {
		if (file != null) {
			String fileName = "D:\\temp\\" + new Date().getTime() + '-' + file.getFileName();
			file.write(fileName);
			service.deploy(new File(fileName));
		}
	}

}
