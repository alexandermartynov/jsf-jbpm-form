package ru.vsu.jmbpm.beans.user;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@ManagedBean
@ViewScoped
public class LoginBean {

	private String username; 
	private String password;
	
	@ManagedProperty(value="#{userBean}")
	private UserBean userBean;

	public void setUserBean(UserBean userBean)
	{
		this.userBean = userBean;
	}
	
	public UserBean getUserBean()
	{
		return userBean;
	}
	
	public String getUsername() 
	{
		return username;
	}
	public void setUsername(String username) 
	{
		this.username = username;
	}
	public String getPassword() 
	{
		return password;
	}
	public void setPassword(String password) 
	{
		this.password = password;
	}
	
	public String login() 
	{
		try 
		{
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
					.getExternalContext().getRequest();
			request.login(username, password);
			userBean.setUser(username);
			return "/main/index.xhtml?faces-redirect=true";
		}
		catch(ServletException e)
		{
			FacesContext.getCurrentInstance().addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ошибка авторизации!", "Не верный логин или пароль."));
			return null;
		}
	}
}
