package ru.vsu.jmbpm.beans.task;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ru.vsu.jmbpm.beans.user.UserBean;
import ru.vsu.jmbpm.services.TaskService;
import ru.vsu.jmbpm.struct.Task;

@ManagedBean
@ViewScoped
public class TaskListBean {
	
	@EJB
	private TaskService taskService;

	@ManagedProperty(value="#{userBean}")
	private UserBean userBean;

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}
	
	public UserBean getUserBean()
	{
		return userBean;
	}
	
	public List<Task> getTasks() 
	{
		return taskService.getTasksByPotentialOwner(userBean.getUsername());
	}
}
